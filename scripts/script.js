/*
Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.


Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, 
соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

*/

function CreateNewUser(name, sname, bday) {
	this.firstName = name, ''
	this.lastName = sname,
		this.birthday = bday
}
CreateNewUser.prototype.getLogin = function (firstName, lastName) {
	return (firstName[0] + lastName)
}

CreateNewUser.prototype.getAge = function (birthday) {
	let year = birthday.slice(6, 10);
	let yearNumber = parseInt(year);

	let today = new Date();
	let nowyear = today.getFullYear();
	return nowyear - yearNumber
	
	// return 2022 - yearNumber;
	

}
CreateNewUser.prototype.getPassword = function (namefunction, birthday) {
	let year = birthday.slice(6, 10);
	return namefunction + year;
}


let user1 = new CreateNewUser(prompt("Введіть Ваше ім'я").toLowerCase(), prompt("Введіть Ваше прізвище").toLowerCase(), prompt("Введіть дату Вашого народження в форматі dd.mm.yyyy"));

console.log(user1)
console.log(user1.getLogin(user1.firstName, user1.lastName));
console.log("Вік користувача: " + user1.getAge(user1.birthday));
console.log("Пароль: " + user1.getPassword(user1.getLogin(user1.firstName, user1.lastName), user1.birthday));

//------------------------------------------------------------------------------------------------------------------

/*Реализовать функцию фильтра массива по указанному типу данных. 
Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null]. */


let filterBy = function (mas, type) {
	let newMas = [];
	for (i = 0; i < mas.length; i++) {
		if (typeof (mas[i]) != type) {
			newMas.push(mas[i])
		}
	}
	console.log(newMas);
}

let diffarr = [11, 26, "one", "two", , true, {}, {}, 23, 45, false];

filterBy(diffarr, "number");